<?php 

require './PHPMailer/PHPMailerAutoload.php';

if ( !empty($_POST) ) {

  
	$nome = $_POST["name"];
	$email = $_POST["email"];
	$telefone = $_POST["phone"];
	$endereco = $_POST["endereco"];
	$mensagem = $_POST["mensagem"];

	$mail = new PHPMailer(true); 

	$mail->CharSet = 'UTF-8';
	$mail->Subject = ("Formulário de Contato principal mobile - Clean House Faxinas");
	
	$mail_body = 		
	"<html>
		<body>
			<p style='font-family: Helvetica, sans-serif; font-size: 18px; line-height: 22px;'>  
				Nome: {$nome}
				<br/>
				E-mail: {$email}
				<br/>
				Telefone: {$telefone}
				<br/>
			   	Endereço: {$endereco}
		       	<br/>
		      	Mensagem: {$mensagem}
		       	<br/>
			</p>
		</body>
	</html>";
	
	$mail->msgHTML($mail_body);

	$mail->SetFrom('no-reply@serv3xceler.com.br', 'Clean House - Faxinas');
	// $mail->addCC( 'contato@kiwipilates.com.br', 'Contato Kiwi Pilates');
	$mail->addCC( 'contato@cleanhousefaxinas.com.br', 'Contato Clean House');


	if ($mail->send()) {
		echo 'Mensagem enviada com sucesso!';
	}  else {
		echo 'Erro o enviar a mensagem: ' . $mail->ErrorInfo;
	}
}
else {
	echo 'Ops!';
}
die;