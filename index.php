<?php get_header(); ?>

<main class="structure">
	<div class="background"></div>
	<section id="homepage">
		<div class="first-bubble"></div>
		<div class="slider">
			<div class="container">
				<div class="row">
					<div class="content">
						<div class="main-title">		
							<h1>Agende sua <p>Faxina</p></h1>
							<p>Em poucos cliques, sem sair de casa. Preencha o formulário abaixo e faça um orçamento gratuito.</p>
							<!--  Mobile -->
							<div class="btn-form">
								<p>Agendar faxina</p>
							</div>
						</div>
						<div class="box-form">
							<p>Para solicitar contato, informe seu interesse e forneça seus dados.</p>
							<select name="servicos-mb" id="servicos-mb" class="select-servicos">
								<option value="empresarial">
									<div class="servicos">
										<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-empresarial.png' ?>" alt=""><p>Serviços Empresariais</p>
									</div>
								</option>
								<option value="residencial">
									<div class="servicos">
										<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-casaterrea.png' ?>" alt=""><p>Serviços Residenciais</p>												
									</div>
								</option>
							</select>
							<form action="" class="form-empresa" data-modal="form-empresa-mobile">	
								<div class="form-steps">
									<p>Onde fica sua empresa?</p>
									<select name="estado" class="estado">
										<option value=""></option>
									</select>
									<select name="cidade" class="cidade">
										<option value="">Escolha uma cidade</option>
									</select>
									<input type="text" name="bairro" placeholder="Bairro">
									
									<p>Qual categoria melhor define sua empresa?</p>

									<select name="categoria" id="empresa">
										<option value="sala">
											<div class="tipo-empresa">
												<p>Sala Comercial</p>
											</div>
										</option>

										<option value="apartamento">
											<div class="tipo-empresa">
												<p>Apartamento</p>
											</div>
										</option>

										<option value="casa">
											<div class="tipo-empresa">
												<p>Casa Térrea</p>
											</div>
										</option>

										<option value="studio">
											<div class="tipo-empresa">
												<p>Stúdio</p>
											</div>
										</option>

										<option value="outros">
											<div class="tipo-empresa">										
												<p>Outros</p>
											</div>
										</option>
									</select>

									<p>Conte-nos mais sobre a sua empresa...</p>
									<input type="text" name="metragem" placeholder="Qual a metragem aproximada?">
									<input type="text" name="frequencia" placeholder="Qual a frequência da Limpeza?">

									<p>Qual o tipo da limpeza?</p>

									<div class="checkbox-form-servicos">
										<input type="checkbox" name="tipo-servico" id="pesada" value="Limpeza Pesada">
										<label for="pesada">Limpeza pesada</label>
									</div>

									<div class="checkbox-form-servicos">
										<input type="checkbox" name="tipo-servico" id="manutencao" value="Limpeza para manutenção">
										<label for="manutencao">Limpeza para manutenção</label>
									</div>
									
									<p>Alguma observação ou necessidade específica na limpeza?</p>
									<textarea name="obs" id="observacao" cols="30" rows="20" placeholder="Quais?"></textarea>
									<p>Informações de contato e agendamento</p>
									
									<input type="text" class="name" name="name" placeholder="Seu Nome" required>
									<input type="phone" class="phone" name="phone" placeholder="Seu Telefone" required>
									<input type="text" class="mail" name="email" placeholder="Seu E-mail" required>
									<input type="text" class="data" name="data" placeholder="Data (dd/mm/aaaa)" required>
									<input type="submit" class="btn-boxsubmit" value="Solicitar Orçamento">
								</div>
							</form>

							<form action="" class="form-residencial" data-modal="form-casa-mobile">	
								
								<div class="form-steps">
									<p>Onde fica sua casa?</p>

									<select name="estado" class="estado">
										<option value=""></option>
									</select>
									<select name="cidade" class="cidade">
										<option value="">Escolha uma cidade</option>
									</select>
									<input type="text" placeholder="Bairro" name="bairro">
									
									<p>Qual categoria melhor define sua casa?</p>

									<select name="categoria" id="casa">
									
										<option value="apartamento">
											<div class="tipo-casa">
												<p>Apartamento</p>
											</div>
										</option>

										<option value="casa">
											<div class="tipo-casa">
												<p>Casa Térrea</p>
											</div>
										</option>

										<option value="studio">
											<div class="tipo-casa">
												<p>Stúdio</p>
											</div>
										</option>
										
										<option value="sala">
											<div class="tipo-casa">
												<p>Outros</p>
											</div>
										</option>
									</select>

									<p>Conte-nos mais sobre a sua casa...</p>
									<input type="text" name="metragem" placeholder="Qual a metragem aproximada?">
									<input type="text" name="frequencia" placeholder="Qual a frequência da Limpeza?">

									<p>Qual o tipo da limpeza?</p>

									<div class="checkbox-form-servicos">
										<input type="checkbox" name="tipo-servico" id="pesada" value="Limpeza Pesada">
										<label for="pesada">Limpeza pesada</label>
									</div>

									<div class="checkbox-form-servicos">
										<input type="checkbox" name="tipo-servico" id="manutencao" value="Limpeza para manutenção">
										<label for="manutencao">Limpeza para manutenção</label>
									</div>
									<p>Alguma observação ou necessidade específica na limpeza?</p>
									<textarea name="obs" id="observacao" cols="30" rows="20" placeholder="Quais?"></textarea>
									<p>Informações de contato e agendamento</p>
									
									<input type="text" class="name" name="name" placeholder="Seu Nome" required>
									<input type="phone" class="phone" name="phone" placeholder="Seu Telefone" required>
									<input type="text" class="mail" name="email" placeholder="Seu E-mail" required>
									<input type="text" class="data" name="data" placeholder="Data (dd/mm/aaaa)" required>
									<input type="submit" class="btn-boxsubmit" value="Solicitar Orçamento">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--  Desk -->
		<div class="container container-form">
			<div class="row">
				<p class="before-box">Para solicitar contato, informe seu interesse e forneça seus dados.</p>
				<div class="col main-box">
					<div class="tabs">
						<div class="tabitem">
							<img class="img inactive" src="<?php echo get_template_directory_uri(). '/img/icons/icon-casaterrea.png' ?>" alt="">
							<img class="img active" src="<?php echo get_template_directory_uri(). '/img/icons/icon-casaterrea2.png' ?>" alt="">
							
							<p>Serviços Residenciais</p>
						</div>
						<div class="tabitem2 inactive">
							<img class="img2 active" src="<?php echo get_template_directory_uri(). '/img/icons/icon-empresarial2.png' ?>" alt="">
							<img class="img2 inactive" src="<?php echo get_template_directory_uri(). '/img/icons/icon-empresarial.png' ?>" alt="">
							<p>Serviços Empresariais</p>
						</div>
					</div>
					<div class="form-box">
						<form action="" class="first-form" id="residencial" data-modal="form-casa">

							<p>Onde fica sua casa?</p>

							<select name="estado" class="estado">
								<option value=""></option>
							</select>
							<select name="cidade" class="cidade">
								<option value="">Escolha uma cidade</option>
							</select>
							<input type="text" class="bairro" name="bairro" placeholder="Bairro" required>

							<p>Qual categoria melhor define seu lar?</p>
							
							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-ap.png'; ?>" alt="Apartamento">
								<input type="checkbox" name="categoria" value="apartamento" id="apartamento">
								<label for="ap">Apartamento</label>
							</div>
							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-casaterrea.png'; ?>" alt="Casa Térrea">
								<input type="checkbox" name="categoria" value="casa" id="casa">
								<label for="casa">Casa Térrea</label>
							</div>

							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-sobrado.png'; ?>" alt="Sobrado">
								<input type="checkbox" name="categoria" value="sobrado" id="sobrado">
								<label for="sobrado">Sobrado</label>
							</div>
							
							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-studio.png'; ?>" alt="Stúdio">
								<input type="checkbox" name="categoria" value="studio" id="studio">
								<label for="studio">Stúdio</label>
							</div>

							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-outros.png'; ?>" alt="Outros">
								<input type="checkbox" name="categoria" value="outros" id="others">
								<label for="others">Outros</label>
							</div>

							<p>Conte-nos mais sobre o seu lar...</p>
							<input type="text" class="lar" name="metragem" placeholder="Qual a metragem aproximada?" required>
							<input type="text" class="lar" name="quarto" placeholder="Quantos quartos possui?" required>
							<input type="text" class="lar" name="banheiro" placeholder="E quantos banheiros?" required>

							<p>Serviços Adicionais?</p>
							
							<div class="checkbox-form-servicos roupa">
								<input type="checkbox" name="tipo-servico" value="roupa" id="roupa">
								<label for="roupa">Passar Roupa</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox" name="tipo-servico" value="geladeira" id="geladeira">
								<label for="geladeira">Limpeza Interna de Geladeira</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox" name="tipo-servico" value="fogao" id="fogao">
								<label for="fogao">Limpeza Interna do Fogão</label>
							</div>

							<div class="checkbox-form-servicos">	
								<input type="checkbox" name="tipo-servico" value="armario" id="armario">
								<label for="armario">Limpeza Interna de Armários</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox" name="tipo-servico" value="organizacao" id="organizacao">
								<label for="organizacao">Serviços de Organização</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox" name="tipo-servico" value="guarda-roupa" id="guarda-roupa">
								<label for="guarda-roupa">Limpeza de Guarda Roupas</label>
							</div>

							<p>Alguma observação ou necessidade específica na limpeza?</p>
							<textarea name="obs"  cols="30" rows="10"></textarea>
							<p>Informações de contato e agendamento:</p>
							<input type="text" class="name" name="nome" placeholder="Seu Nome" required>
							<input type="phone" class="phone" name="telefone" placeholder="Seu Telefone" required>
							<input type="text" class="mail" name="email" placeholder="Seu E-mail" required>
							<input type="text" class="data" name="data" placeholder="Data (dd/mm/aaaa)">
							<input type="submit" class="btn-boxsubmit" value="Solicitar Orçamento">
						</form>

						<form action="" class="first-form first" id="empresarial" data-modal="form-empresa">

							<p>Onde fica sua empresa?</p>

							<select name="estado" class="estado">
								<option value=""></option>
							</select>
							<select name="cidade" class="cidade">
								<option value="">Escolha uma cidade</option>
							</select>
							<input type="text" class="bairro" placeholder="Bairro" name="bairro" required>

							<p>Qual categoria melhor define sua empresa?</p>
							
							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-ap.png'; ?>" alt="Apartamento">
								<input type="checkbox" name="categoria" value="apartamento" id="apartamento">
								<label for="ap" class="btn-label">Apartamento</label>
							</div>
							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-casaterrea.png'; ?>" alt="Casa Térrea">
								<input type="checkbox" name="categoria" value="casa" id="casa">
								<label for="casa" class="btn-label">Casa Térrea</label>
							</div>

							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-sobrado.png'; ?>" alt="Sobrado">
								<input type="checkbox" name="categoria" value="sobrado" id="sobrado">
								<label for="sobrado" class="btn-label">Sobrado</label>
							</div>
							
							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-studio.png'; ?>" alt="Stúdio">
								<input type="checkbox" name="categoria" value="studio" id="studio">
								<label for="studio" class="btn-label">Stúdio</label>
							</div>

							<div class="checkbox-form">
								<img class="icon" src="<?php echo get_template_directory_uri(). '/img/icons/icon-outros.png'; ?>" alt="Outros">
								<input type="checkbox" name="categoria" value="outros" id="others">
								<label for="others" class="btn-label">Outros</label>
							</div>

							<p>Conte-nos mais sobre a sua empresa...</p>
							<input type="text" class="lar" name="metragem" placeholder="Qual a metragem aproximada?" required>
							<input type="text" class="lar" name="quarto" placeholder="Quantos quartos possui?" required>
							<input type="text" class="lar" name="banheiro" placeholder="E quantos banheiros?" required>

							<p>Serviços Adicionais?</p>

							
							<div class="checkbox-form-servicos roupa">
								<input type="checkbox" name="tipo-servico" value="roupa" id="roupa">
								<label for="roupa" id="btn-label">Passar Roupa</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox" name="tipo-servico" value="geladeira" id="geladeira">
								<label for="geladeira" class="btn-label">Limpeza Interna de Geladeira</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox" name="tipo-servico" value="fogao" id="fogao">
								<label for="fogao" class="btn-label">Limpeza Interna do Fogão</label>
							</div>

							<div class="checkbox-form-servicos">	
								<input type="checkbox" name="tipo-servico" value="armario" id="armario">
								<label for="armario" class="btn-label">Limpeza Interna de Armários</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox"  name="tipo-servico" value="organizacao" id="organizacao">
								<label for="organizacao" class="btn-label">Serviços de Organização</label>
							</div>

							<div class="checkbox-form-servicos">
								<input type="checkbox"  name="tipo-servico" value="guarda-roupa" id="guarda-roupa">
								<label for="guarda-roupa" class="btn-label">Limpeza de Guarda Roupas</label>
							</div>

							<p>Alguma observação ou necessidade específica na limpeza?</p>
							<textarea name="obs" id="" cols="30" rows="10"></textarea>
							<p>Informações de contato e agendamento:</p>
							<input type="text" class="name" name="nome" placeholder="Seu Nome" required>
							<input type="phone" class="phone" name="telefone" placeholder="Seu Telefone" required>
							<input type="text" class="mail" name="email" placeholder="Seu E-mail" required>
							<input type="text" class="data" name="data" placeholder="Data (dd/mm/aaaa)">
							<input type="submit" class="btn-boxsubmit" value="Solicitar Orçamento">
						</form>
					</div>
				</div>
			</div>
		</div>	
	</section>
	
	<section id="services">	
		<!-- sides - bubbles -->
		<div id="bubble" class="left-bubble bubble"
			data-paroller-factor="-0.5"
			data-paroller-direction="vertical"></div>

		<!-- bubble-mobile -->
		<div class="left-bubble-mb bubble"
			data-paroller-factor="-0.5"
			data-paroller-direction="vertical"></div>


		<div id="bubble" class="right-bubble bubble"
			data-paroller-factor="-0.5"
			data-paroller-direction="vertical"></div>

		<div id="bubble" class="bubble-form">
			<div class="left-bubble-med bubble" 
			data-paroller-factor="-0.5"
			data-paroller-direction="vertical"
			></div>

			<div class="bottom-bubble-med bubble" 
			data-paroller-factor="-0.5"
			data-paroller-direction="vertical"
			></div>
		</div>
		<div class="container box-servicos">
			<div class="row">
				<div class="sub-title">
					<h2>Serviços que Agilizam <p class="blue-title">o seu dia a dia</p></h2>
				</div>
				<div class="content-services">
					<div class="row">
						<p>Chegar em casa e encontrar seu lar com uma pilha de louças para lavar; móveis para limpar; roupas para passar ou comida para fazer é o pesadelo de muitas pessoas, não é? Com a Clean House, você voltará para a sua casa com as principais tarefas domésticas já feitas! Veja abaixo os serviços que oferecemos para agilizar o seu dia a dia! </p>
					</div>
					<div class="carousel-first">
						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/faxineira.png'; ?>" alt="">
								</div>
								<h3>Faxineira</h3>
								<p class="desc-servico">Manter a casa limpa e organizada não é uma tarefa fácil, quando se esbarra na dificuldade de encontrar profissionais dedicados e pontuais.</p>
							</div>
						</div>	
						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/limpeza.png'; ?>" alt="">
								</div>
								<p class="pre-title">Auxiliar de</p>
								<h3 class="text-servico">Limpeza</h3>
								<p class="desc-servico">A limpeza profissional consiste na utilização de técnicas, produtos, equipamentos e qualificação dos profissionais envolvidos, diminuindo assim os possíveis riscos e impactos ambientais em suas atividades.</p>
							</div>
						</div>
						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/passadeira.png'; ?>" alt="">
								</div>
								<h3>Passadeira</h3>
								<p class="desc-servico">Nada incomoda mais do que chegar em casa após um dia exaustivo de trabalho e ter uma pilha de roupa para passar. O acumulo de roupas no pós-viagem, bem como em situações comuns do dia a dia às vezes exige reforços.</p>
							</div>
						</div>
						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/cozinheira.png'; ?>" alt="">
								</div>
								<h3>Cozinheira</h3>
								<p class="desc-servico">O segredo para uma celebração tranquila ou algum momento especial está na experiência e profissionalismo de quem conduz a cozinha. E para isso é necessário um profissional responsável pela execução das atividades de confecção de pratos, almoços, jantares ou eventos.</p>
							</div>
						</div>

						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/pos-obras.png'; ?>" alt="">
								</div>
								<p class="pre-title">Limpeza</p>
								<h3 class="text-servico">Pós-obras</h3>
								<p class="desc-servico">Serviço voltado para a limpeza e reparos de construções e reformas. Ideal para deixar a casa ou empresa pronta para a mudança após o fim das obras.</p>
							</div>
						</div>
						
						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/piscina.png'; ?>" alt="">
								</div>
								<p class="pre-title">Limpador de</p>
								<h3 class="text-servico">Piscina</h3>
								<p class="desc-servico">É extremamente necessário que você se preocupe com a limpeza constante da piscina seja ela coberta ou não. O ideal é realizar manutenções semanais para evitar que a água fique totalmente suja (turva) trazendo problemas prejudiciais a sua saúde.</p>
							</div>
						</div>

						<div class="col-1">
							<div class="item-servico">
								<div class="circle">
									<img src="<?php echo get_template_directory_uri(). '/img/jardineiro.png'; ?>" alt="">
								</div>
								<h3>Jardineiro</h3>
								<p class="desc-servico">O jardim é um dos espaços mais belos de uma casa, condomínio, clube ou empresa. É um lugar onde as pessoas podem passear e desfrutar de bons momentos de descanso, um ambiente agradável, um lindo enfeite natural. E por se tratar de um lugar que requer uma atenção especial é primordial saber quem você está contratando.</p>
							</div>
						</div>
					</div>
					<div class="carousel-info">
						<p><strong> << </strong> Deslize para ver mais. <strong> >> </strong></p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section id="clean-house">

		<div id="bubble" class="bubble-right-work bubble"
			data-paroller-factor="-0.5"
			data-paroller-direction="vertical"></div>
		
		<div class="container box-clean">
			<div class="row">
				<div class="clean-title">		
					<h4>Nosso trabalho é <p>Facilitar a sua vida!</p></h4>
				</div>
				<div class="col box-clean">
					<div class="text-clean">
						<p>Nossa missão é tornar a sua vida mais fácil e prática. Depois de um longo dia de trabalho, nada melhor do que chegar em casa e encontrar seu lar bem limpinho e agradável para um belo descanso, não é? </p>

						<h5>Deixamos seu lar ainda mais acolhedor</h5>

						<p>Contamos com uma equipe de colaboradores experientes e muito bem treinados e nosso time trabalha para ser a solução ideal para sua casa ou empresa! Com os serviços de faxineira; auxiliar de limpeza; passadeira; cozinheira; limpeza pós-obras; limpador de piscinas e jardinagem, facilitamos as principais tarefas do seu dia a dia. </p>

						<p>Você quer conforto e praticidade? Então, escolha o serviço que precisa e te entregamos o melhor atendimento. Queremos fazer da sua residência, um lar ainda mais acolhedor! Sendo assim, ao chegar em casa, sua única preocupação será recarregar suas energias com um merecido descanso!</p>
					</div>
				</div>
				<div class="clean-subtitle">		
					<h4>Porque Contratar a 	<p>Clean House?</p></h4>
				</div>
				<div class="carousel-first">
					<div class="col-sm">
						<div class="circle-clean">

							<img src="<?php echo get_template_directory_uri(). '/img/burocracia.png'; ?>" alt="">
						</div>
						<div class="item-clean">			
							<h5 class="first-title-clean">Contratação sem burocracia</h5>
							<p class="desc-clean">Informe o número de cômodos e banheiros do seu ambiente. Escolha uma data e pronto, é só aguardar seu orçamento e confirmar o serviço.</p>	
						</div>
					</div>
					<div class="col-sm">
						<div class="circle-clean">
							<img src="<?php echo get_template_directory_uri(). '/img/servico.png'; ?>" alt="">			
						</div>
						<div class="item-clean">
							<h5>Serviço Eficiente</h5>
							<p class="desc-clean">Contamos com uma equipe de profissionais experientes e treinados. Nossa equipe trabalha para ser a solução da sua casa ou empresa na prestação de serviços de limpeza.</p>
						</div>
					</div>
					<div class="col-sm">
						<div class="circle-clean">
							<img src="<?php echo get_template_directory_uri(). '/img/preco.png'; ?>" alt="">
						</div>
						<div class="item-clean">
							<h5>Preço Justo</h5>
							<p class="desc-clean">Trabalhamos de forma personalizada de acordo com a necessidade de cada cliente. Solicite um orçamento sem compromisso.</p>
						</div>
					</div>
				</div>
				<div class="carousel-info">
					<p><strong> << </strong> Deslize para ver mais. <strong> >> </strong></p>
				</div>
			</div>
			<div class="img-aside">
				<img src="<?php echo get_template_directory_uri(). '/img/img-side.png'; ?>" alt="">
			</div>
		</div>
	</div>
</section>
<section id="contact">

	<div class="bubble-contact">

		<div class="bubble-contact-left bubble"
 		data-paroller-factor="-0.5"
		data-paroller-direction="vertical"></div>
		<div class="bubble-contact-right bubble"
 		data-paroller-factor="-0.5"
		data-paroller-direction="vertical"></div>
	</div>


	<div class="container box-contact">
		<div class="row">
			<div class="clean-title">		
				<h4>Entre em <p>Contato</p></h4>
			</div>
			<div class="col box-clean">
				<div class="text-clean info-contact">
					<p>Quer uma solução prática e eficiente para as tarefas domésticas do dia a dia? Entre em contato e tire as suas dúvidas sobre os serviços que disponibilizamos para facilitar a sua rotina!</p>
					<p>Tem alguma dúvida?</p>
					<h5>Ligue pra Nós!</h5>
					<div class="contact-phone">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/phone-contact.png'; ?>"  class="icon-contact" alt="Telefones para Contato">
						<p><a href="tel:+551150442276">11 5044.2276</a> das 8h às 19h.</p>
						<!-- <a href="tel:+551144443333">11 4444.3333</a> -->
					</div>
					<p>Envie um</p>
					<h5>Whatsapp!</h5>
					<div class="contact-whats">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/zap-contact.png'; ?>" class="icon-contact" alt="Whatsapp para Contato">
						<p>SP: <a href="tel:+5511977752614">11 9 7775.2614</a> das 7h às 22h.</p>

			<!-- 			<p>RJ: <a href="tel:+5521999998888">21 99999.8888</a></p> -->
					</div>
				</div>
			</div>	

			<div class="col-2 contact-desk">
				<div class="contact-form">
					<form action="" data-modal="form-contato">
						<div class="form-item">
							<label for="name">Nome:</label>
							<input type="text" name="name" required>
						</div>

						<div class="form-item">
							<label for="email">E-mail:</label>
							<input type="mail" name="email" required>
						</div>

						<div class="form-item">
							<label for="phone">Telefone:</label>
							<input type="phone" name="phone" required>
						</div>

						<div class="form-item">
							<label for="address">Endereço:</label>
							<input type="text" name="endereco">
						</div>

						<div class="form-item">
							<label for="assunto">Assunto:</label>
							<select name="assunto" id="assunto">
								<option value="">Escolha uma opção</option>
								<option value="Serviços">Serviços</option>
								<option value="Duvidas">Dúvidas</option>
								<option value="Outros">Outros</option>

							</select>
						</div>
						<div class="form-item">
							<label for="message">Mensagem:</label>
							<textarea name="mensagem" id="" cols="47" rows="5" resizable="false"></textarea>
						</div>
						<button type="submit" class="btn-submit">
							<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-fly.png'; ?>">
							Enviar
						</button>
					</form>
				</div>
			</div>
			<div class="col-2 contact-mobile">
				<div class="contact-form">
					<form action="" data-modal="form-contato-mobile">
						<div class="form-item">
							<label for="name">Nome:</label>
							<input type="text" name="name" required>
						</div>

						<div class="form-item">
							<label for="email">E-mail:</label>
							<input type="mail" name="email" required>
						</div>

						<div class="form-item">
							<label for="phone">Telefone:</label>
							<input type="phone" name="phone" required>
						</div>

						<div class="form-item">
							<label for="address">Endereço:</label>
							<input type="text" name="endereco">
						</div>

						<div class="form-item">
							<label for="message">Mensagem:</label>
							<textarea name="mensagem" id="" cols="20" rows="10" resizable="false"></textarea>
						</div>
						<button type="submit" class="btn-submit">
							<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-fly.png'; ?>">
							Enviar
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>		
</main>
<?php get_footer(); ?>