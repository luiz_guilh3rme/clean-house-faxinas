// unique scripts go here.
function loadMask(){
	$('.data').mask('00/00/0000');
	$('input[type="phone"]').mask('(00) 00000-0000');
	$('.box-date').mask('00H00');
}
//não é corega, mas é
function tabs(){
	$('#residencial').css('display','block');
	$('#empresarial').css('display', 'none');

	$('.tabitem .inactive').css('display','none');
	$('.tabitem2 .active').css('display','none');

	$('.tabitem').click(function(){

		$('.img.active').show();
		$('.img.inactive').hide();

		$('.img2.inactive').show();
		$('.img2.active').hide();

		$(this).removeClass('inactive');
		$('.tabitem2').addClass('inactive');
		
		//forms
	
		$('#empresarial').fadeOut('fast');
		$('#residencial').fadeIn('slow');

	});

	$('.tabitem2').click(function(){
		$('.img2.inactive').hide();
		$('.img2.active').show();

		$('.img.inactive').show();
		$('.img.active').hide();

		$(this).removeClass('inactive');
		$('.tabitem').addClass('inactive');			

		//forms
		$('#residencial').fadeOut('fast');
		$('#empresarial').fadeIn('slow');
		
	});

}

// is not geometry, but we have
function formsOptions(){
	var servico = document.querySelector('#servicos-mb');
	servico.addEventListener('change', function(e){

		if(servico.value === 'empresarial') {
			$('.form-residencial').fadeOut('fast');
			$('.form-empresa').fadeIn('fast');

		}else{
			$('.form-empresa').fadeOut('fast');
			$('.form-residencial').fadeIn('fast');
		}
	});
}

// old but
function goldButton(){
var checkbox = $('.checkbox-form');
var label = $('.btn-label');

	checkbox.on('click',function (){
		$(this).toggleClass('btn-active');

	});

var checkservice = $('.checkbox-form-servicos');
	checkservice.on('click', function(){
		$(this).toggleClass('btn-active');
	});

}

//isn't smooth criminal, its
function smoothBubble(){

	$('.bubble').paroller();


}

function loadForm(){
	$('.btn-form').click(function(){

		$('.box-form').fadeToggle('fast','linear');
	
	})
}

function scrollAnchor(){
	//desk
	$('.item a').click(function(e){
		e.preventDefault();
		$('html, body').animate({
			scrollTop:$( $(this).attr('href')).offset().top }, 1000, 'linear');
	});
	//mobile
	$('.overlay-menu a').click(function(e){
		e.preventDefault();
  			$('body, html').css('overflow-y','initial');
  			$('.outline-menu').fadeOut('fast');
			$('.btn-menu').removeClass('change');
		$('html, body').animate({
			scrollTop:$( $(this).attr('href')).offset().top }, 1500, 'linear');
	});

}

function animateButton(){

	$('.btn-menu').click(function(){
	    $(this).toggleClass('change');
	    if($(this).hasClass('change') ){

		    $('body, html').css('overflow','hidden');	   
    		$('.outline-menu').fadeIn('fast');
    		$('.box-form').fadeOut('fast');
    		
	    }else{

		  	$('body, html').css('overflow-y','initial');
		  	$('body, html').css('overflow-x', 'hidden');

			$('.outline-menu').fadeOut('fast');
    		
	    }
	});
}

function sliderMobile(){

	  $('.carousel-first').slick({
	  		arrows:false,
	  		slidesToScroll: 1,
	  		mobileFirst:true,
	  		responsive:[{
	  			breakpoint: 1024,
	  			settings: "unslick"
	  		}
	  		]
	  });
}

