<?php 

require './PHPMailer/PHPMailerAutoload.php';

if ( !empty($_POST) ) {

	$data = $_POST["data"];
	$hora = $_POST["hora"];
	$nome = $_POST["nome"];
	$telefone = $_POST["telefone"];

	$mail = new PHPMailer(true); 

	$mail->CharSet = 'UTF-8';
	$mail->Subject = ("Formulário de contato Popup me ligue depois - Clean House Faxinas");
	$mail->msgHTML(
	"<html>
		<body>
			<p style='font-family: Helvetica, sans-serif; font-size: 18px; line-height: 22px;'>  
				Data: {$data}
				<br/>
				Hora: {$hora}
				<br/>
				Nome: {$nome}
				<br/>
				Telefone:{$telefone}
				<br/>
			</p>
		</body>
	</html>");
	$mail->SetFrom('no-reply@serv3xceler.com.br', 'Clean House Faxinas');
	// $mail->addCC( 'contato@kiwipilates.com.br', 'Contato Kiwi Pilates');
	$mail->addCC( 'contato@cleanhousefaxinas.com.br', 'Contato Clean House');


	if ($mail->send()) {
		echo 'Mensagem enviada com sucesso!';
	}  else {
		echo 'Erro o enviar a mensagem: ' . $mail->ErrorInfo;
	}
}
else {
	echo 'Ops!';
}
die;