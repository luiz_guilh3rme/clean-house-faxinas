<?php 

require './PHPMailer/PHPMailerAutoload.php';

if ( !empty($_POST) ) {


	$estado = trim($_POST["estado"]);
	$cidade = trim($_POST["cidade"]);
	$bairro = trim($_POST["bairro"]);
	$categoria = $_POST["categoria"]; 
	$metragem = trim($_POST["metragem"]);
	$quarto = trim($_POST["quarto"]);
	$servicos = $_POST["tipo-servico"];
	$obs = trim($_POST["obs"]);
	$nome = trim($_POST["nome"]);
	$telefone = trim($_POST["telefone"]);
	$email = trim($_POST["email"]);
	$data = trim($_POST["data"]);

	$categorias_string = ''; 

	foreach ( $categoria as $cat ) {
		$categorias_string .= ' ' .$cat.  '  <br>';
	}

	$servicos_string = ''; 

	foreach ($servicos as $servico) {
		$servicos_string .= ' ' .$servico.  '  <br>';
	}
	

	$mail = new PHPMailer(true); 

	$mail->msgHTML("<html>
		<body>
		<p style='font-family: Helvetica, sans-serif; font-size: 18px; line-height: 22px;'>  
		Nome: {$nome}
		<br/>
		E-mail: {$email}
		<br/>
		Telefone: {$telefone}
		<br/>
		Data: {$data} 
		<br/> 
		Estado: {$estado}
		<br/>
		Cidade: {$cidade}
		<br/>
		Bairro: {$bairro}
		<br/>
		Categoria: <br> {$categorias_string}
		<br/>
		Metragem: {$metragem}
		<br/>
		Quarto: {$quarto}
		<br/>
		Serviços: <br> {$servicos_string}
		<br/>
		Observação: {$obs}
		</p>
		</body>
		</html>");
	
	$mail->CharSet = 'UTF-8';
	$mail->Subject = ("Formulário de contato Principal - Clean House Faxinas");
	// $mail->msgHTML();

	$mail->SetFrom('no-reply@serv3xceler.com.br', 'Clean House Faxinas');
	// $mail->addCC( 'contato@kiwipilates.com.br', 'Contato Kiwi Pilates');
	$mail->addCC( 'contato@cleanhousefaxinas.com.br', 'Contato Clean House');


	if ($mail->send()) {
		echo 'Mensagem enviada com sucesso!';
	}  else {
		echo 'Erro o enviar a mensagem: ' . $mail->ErrorInfo;
	}
}
else {
	echo 'Ops!';
}
die;