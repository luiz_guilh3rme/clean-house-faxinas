<?php 

require './PHPMailer/PHPMailerAutoload.php';

if ( !empty($_POST) ) {

   	$estado = trim($_POST["estado"]);
	$cidade = trim($_POST["cidade"]);
	$bairro = trim($_POST["bairro"]);
	$categoria = $_POST["categoria"];
	$metragem = trim($_POST["metragem"]);
	$frequencia = trim($_POST["frequencia"]);
	$servicos = $_POST["tipo-servico"];
	$obs = trim($_POST["obs"]);
	$nome = trim($_POST["name"]);
	$telefone = trim($_POST["phone"]);
	$email = trim($_POST["email"]);
	$data = trim($_POST["data"]);

	$mail = new PHPMailer(true); 

	$mail->CharSet = 'UTF-8';
	$mail->Subject = ("Formulário de contato Principal - Clean House Faxinas");

	$mail_body =
	"<html>
		<body>
			<p style='font-family: Helvetica, sans-serif; font-size: 18px; line-height: 22px;'>  
				Nome: {$nome}
				<br/>
				E-mail: {$email}
				<br/>
				Telefone: {$telefone}
				<br/>
				Data: {$data} 
		        <br/> 
		       	Estado: {$estado}
		       	<br/>
		       	Cidade: {$cidade}
		       	<br/>
		       	Bairro: {$bairro}
		       	<br/>
		       	Categorias: <br> {$categoria}
				<br/>
				Metragem: {$metragem}
				<br/>
				Frequência: {$frequencia}
				<br/>
				Serviços: <br> {$servicos}
				<br/>
				Observação: {$obs}
			</p>
		</body>
	</html>";
	$mail->msgHTML($mail_body);



	$mail->SetFrom('no-reply@serv3xceler.com.br', 'Clean House Faxinas');
	// $mail->addCC( 'contato@kiwipilates.com.br', 'Contato Kiwi Pilates');
	$mail->addCC( 'contato@cleanhousefaxinas.com.br', 'Contato Clean House');


	if ($mail->send()) {
		echo 'Mensagem enviada com sucesso!';
	}  else {
		echo 'Erro o enviar a mensagem: ' . $mail->ErrorInfo;
	}
}
else {
	echo 'Ops!';
}
die;