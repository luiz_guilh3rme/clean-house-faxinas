<?php 
/* 
Template Name: Redirecting WhatsApp

*/

get_header();
?>
<section id="obrigado">
	<div class="content-ty">
		<div class="container">
			<div class="row">
				<div class="ty-page">
					<div class="title-tks">
						<?php echo get_post_field('post_content', $post_id); ?>		
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	setTimeout(function(){
	 window.location.href = 'https://wa.me/5511977752614';
         }, 2000);
</script>

<?php get_footer(); ?>