<head>
    <meta charset="UTF-8">
    <title>
        <?php echo get_bloginfo('name'); ?>
    </title>
    <meta name="description" content="Clean House Faxinas">
    <meta name="language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="3xceler - Marketing Performance">
    <meta name="language" content="pt-br" />
    <link rel="canonical" href="<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>" />
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KD8PBSL');</script>
    <!-- End Google Tag Manager -->
    <?php wp_head(); ?>
</head>
<header>
<!-- Mobile menu -->
<div class="topmenu">
    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(). '/img/logomobile.png' ?>" alt="Logotipo Clean House"></a>
</div>
<div class="btn-menu">
 <div class="bar1"></div>
 <div class="bar2"></div>
 <div class="bar3"><div class="bar3-right"></div></div>
</div>
<div class="outline-menu">
    <div class="menu-open">
        <nav class="overlay-menu">
            <ul>
                <li><a href="#services">Serviços</a></li>
                <li><a href="#clean-house">Quem somos</a></li>
                <li><a href="#contact">Contato</a></li>
            </ul>
        </nav>
    </div>
</div>
<!-- end -->
<div class="container">
    <div class="row">

        <div class="topinfo">
            <div class="topleft">

                <img src="<?php echo get_template_directory_uri(). '/img/icons/icon-phone.png'; ?>" class="icon-top" alt="11 5044-2276">
                <a href="tel:+551150442276">
                    <p><strong> 11 5044.2276</strong></p>
                </a>

                <img src="<?php echo get_template_directory_uri(). '/img/icons/icon-mail.png'; ?>" class="icon-top" alt="contato@cleanhousefaxinas.com.br">
                <a href="mailto:contato@cleanhousefaxinas.com.br">
                   <p>contato@cleanhousefaxinas.com.br</p>
               </a>
           </div>

           <div class="cover-phone">
            <div class="topright">
                <img src="<?php echo get_template_directory_uri(). '/img/icons/icon-zap.png'; ?>" class="zap" alt="11 97775-2614">
                <p><a href="wpp" target="_blank">11 97775.2614</a></p>       
        </div>
    </div>
</div>
<nav>
   <div class="main-menu">     
    <div class="item menu-left">
        <ul>
            <li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li><a href="#services">Serviços</a></li>
        </ul>
    </div>
    <div class="logocenter">
        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(). '/img/logo.png'; ?>" alt="Logotipo Clean House Faxinas"></a>
    </div>
    <div class="item menu-right">       
        <ul>
            <li><a href="#clean-house">Quem somos</a></li>
            <li><a href="#contact">Contato</a></li>
        </ul>
    </div>
</div>
</div>
</div>

</nav>
</header>
