<footer>

	<div class="container">
		<div class="row">
			<div class="footer-box">
				<div class="footer-left">
					<p>Clean House Faxinas © 2019 </p><p>- Todos os direitos reservados.</p>
				</div>
				<div class="footer-right">
					<p>Criação de Sites:</p>
					<img src="<?php echo get_template_directory_uri(). '/img/logo-3xceler.png'; ?>" alt="Logo 3xceler">
				</div>
			</div>
		</div>
	</div>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KD8PBSL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php
get_template_part('includes/components/mobile-cta-wrapper');
get_template_part('includes/components/cta-wrapper');
?>
</footer>
<?php 
get_template_part('includes/components/oldie'); 
wp_footer();
?>
</body>
</html> 
