<?php 
	// Template Name: Obrigado - Boxes

include 'header.php';
?>
<section id="obrigado">
	<div class="content-ty">
		<div class="container">
			<div class="row">
				<div class="ty-page">
					<div class="title-tks">
						<?php echo get_post_field('post_content', $post_id); ?>		
						<p class="btn-back"><a href="<?php echo home_url(); ?>">Clique aqui</a> para voltar a página principal.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>