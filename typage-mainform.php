<?php 
	// Template Name: Obrigado - Formulário Principal

include 'header.php';
?>
	<div class="container">
		<div class="content ty-page">
			<div class="title-tks">
				<?php echo get_post_field('post_content', $post_id); ?>		
				<p class="btn-back"><a href="<?php echo home_url(); ?>">Clique aqui</a> para voltar a página principal.</p>
			</div>
		</div>
	</div>

	
<?php include 'footer.php'; ?>