    $("input[name='phone'], input[name='cellphone']")
    .mask("(99) 9999-9999?9")
    .focusout(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    });
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $(document).ready(function () {
        var templateUrl = object_name.templateUrl;
        var origin = window.location.origin;

        $('body').on('submit', 'form', function (event) {
            event.preventDefault();

            // set generic variables

            var fields = $(this).serializeObject(),
            instance = $(this).data('modal'),
            button = $(this).children('[type="submit"]'),
            mailInstance,
            redirect;

            // sort form instance
            switch (instance) {

                // main-form
                case 'form-empresa-mobile':
                mailInstance = templateUrl + '/ajax/main-form-mobile.php';
                // redirect = origin + 'obrigado-formulario-principal/';
                redirect = origin + '/obrigado/'
                break;

                case 'form-casa-mobile':
                mailInstance = templateUrl + '/ajax/main-form-mobile.php';
                // redirect = origin + 'obrigado-formulario-principal/';
                redirect = origin + '/obrigado/'
                break;

                case 'form-casa':
                mailInstance = templateUrl + '/ajax/main-form-desk.php';
                // redirect = origin + 'obrigado-formulario-principal/';
                 redirect = origin + '/obrigado/'
                break;

                case 'form-empresa':
                mailInstance = templateUrl + '/ajax/main-form-desk.php';
                // redirect = origin + 'obrigado-formulario-principal/';
                 redirect = origin + '/obrigado/'
                break;

                // contact-form (footer)
                case 'form-contato':
                mailInstance = templateUrl + '/ajax/form-contact.php';
                // redirect = origin + 'obrigado-contato/';
                 redirect = origin + '/obrigado/'
                break;

                case 'form-contato-mobile':
                mailInstance = templateUrl + '/ajax/form-contact-mobile.php';
                // redirect = origin + 'obrigado-contato/';
                 redirect = origin + '/obrigado/'
                break;

                // boxes
                case 'box-nos-te-ligamos':
                mailInstance = templateUrl + '/ajax/box-call-now.php';
                // redirect = origin + 'obrigado-boxes/';
                 redirect = origin + '/obrigado/'
                break;

                case 'box-agendar-horario':
                mailInstance = templateUrl + '/ajax/box-call-later.php';
                // redirect = origin + 'obrigado-boxes/';
                 redirect = origin + '/obrigado/'         
                break;  

                case 'box-deixar-mensagem':
                mailInstance = templateUrl + '/ajax/box-message.php';
                // redirect = origin + 'obrigado-boxes/';
                 redirect = origin + '/obrigado/'
                break;

                default:
                return false;
            }

            // ajax to email submit
            $.ajax({
                url: mailInstance,
                type: 'POST',
                data: fields,
            })
            .done(function (r) {
                swal({
                    title: 'Obrigado pelo Contato!',
                    text: 'Nossos Analistas irão responder o mais cedo possível!',
                    icon: 'success',
                });
                setTimeout(function () {
                        window.location =  redirect;
                    }, 2000);
            })
            .fail(function () {
                swal({
                    title: 'Algo deu errado...',
                    text: 'Tente novamente mais tarde ou entre em contato através de nossos telefones.',
                    icon: 'error',
                })
            })
            .always(function () {
                console.log(fields);
            });
        });
    });




