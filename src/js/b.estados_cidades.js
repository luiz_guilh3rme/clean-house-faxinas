	
	function loadEstados(){
		var jsonURL = list_state.templateUrl + '/dist/estados_cidades.json';

		$.getJSON(jsonURL, function (data) {
	
			var estados = data;
			var items = [];
			var options = '<option value="">Escolha um estado</option>';	

			$.each(data, function (key, val) {
				options += '<option data-key="' + key +'" value="' + val.nome + '">' + val.nome + '</option>';
			});					

			$('.estado').html(options);
			$('.estado').change(function () {				
			
				var cidades = '';
				var key = $(this).find('option:selected').val();
				estados.forEach( function(estado){
				 	if(estado.nome === key){
				 		cidades = estado.cidades;
				 	}
				});
				var html ='';	
				cidades.forEach(function(cidade){
					console.log(cidade);
					html = html + '<option value="' + cidade + '">'+ cidade + '</option>';
			 	});
				$('select[name="cidade"]').html(html);
			 });		

		});
	}


